FROM cloudron/base:5.0.0@sha256:04fd70dbd8ad6149c19de39e35718e024417c3e01dc9c6637eaf4a41ec4e596c

WORKDIR /app/code
RUN mkdir -p /app/code

# Go (https://golang.org/dl/)
ARG GOVERSION=1.22.9
ENV GOROOT=/usr/local/go-${GOVERSION}
ENV PATH=$GOROOT/bin:$PATH
RUN mkdir -p /usr/local/go-${GOVERSION} && \
    curl -L https://storage.googleapis.com/golang/go${GOVERSION}.linux-amd64.tar.gz | tar zxf - -C /usr/local/go-${GOVERSION} --strip-components 1
RUN ln -sf /usr/local/go-${GOVERSION} /usr/local/go

RUN corepack enable pnpm

# renovate: datasource=github-releases depName=apache/answer versioning=semver extractVersion=^v(?<version>.+)$
ARG ANSWER_VERSION=1.4.2
# renovate: datasource=github-tag depName=apache/answer-plugins versioning=semver extractVersion=^cache-redis/v(?<version>.+)$
ARG REDIS_PLUGIN_VERSION=1.3.0
# renovate: datasource=github-tag depName=apache/answer-plugins versioning=semver extractVersion=^connector-basic/v(?<version>.+)$
ARG CONNECTOR_PLUGIN_VERSION=1.2.9

# the plugin versions above are unused (not sure how to use them)
RUN mkdir -p /tmp/build && \
    curl -L https://github.com/apache/answer/releases/download/v${ANSWER_VERSION}/apache-answer-${ANSWER_VERSION}-bin-linux-amd64.tar.gz | tar -xz --strip-components=1 -C /tmp/build && \
    /tmp/build/answer build \
        --with github.com/apache/answer-plugins/cache-redis \
        --with github.com/apache/answer-plugins/connector-basic \
        --with github.com/apache/answer-plugins/connector-google \
        --output /app/code/answer && \
    rm -rf /tmp/build

RUN ln -sf /app/data/data /data

COPY start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
