#!/bin/bash

set -eu

export SKIP_SMTP_TLS_VERIFY=true

DB_CMD="mysql --user=${CLOUDRON_MYSQL_USERNAME} --password=${CLOUDRON_MYSQL_PASSWORD} --host=${CLOUDRON_MYSQL_HOST} ${CLOUDRON_MYSQL_DATABASE}"
DB_CMD_OPTIONS="${DB_CMD} -e"
DB_CMD_OPTIONS_SELECT="${DB_CMD} -N -B -e"

wait_for_app() {
    # Wait for app to come up
    while ! curl --fail -s http://localhost > /dev/null; do
        echo "=> Waiting for app to come up"
        sleep 1
    done
}

if [[ ! -f /app/data/data/conf/config.yaml ]]; then
    echo "==> Init database on first run"
    /app/code/answer init -C /app/data/data &
    pid=$!
    sleep 2

    ( wait_for_app )

    curl -s -X POST -H 'content-type: application/json' -d "{\"lang\":\"en_US\",\"db_type\":\"mysql\",\"db_username\":\"${CLOUDRON_MYSQL_USERNAME}\",\"db_password\":\"${CLOUDRON_MYSQL_PASSWORD}\",\"db_host\":\"${CLOUDRON_MYSQL_HOST}:${CLOUDRON_MYSQL_PORT}\",\"db_name\":\"${CLOUDRON_MYSQL_DATABASE}\",\"db_file\":\"/app/data/data/answer.db\"}" 'http://localhost/installation/init'

    sleep 2
    # create admin account
    curl -s -X POST -H 'content-type: application/json' -d "{\"lang\":\"en_US\",\"site_name\":\"Answer\",\"site_url\":\"${CLOUDRON_APP_ORIGIN}\",\"contact_email\":\"admin@cloudron.local\",\"login_required\":true,\"name\":\"admin\",\"password\":\"changeme123\",\"email\":\"admin@cloudron.local\"}" 'http://localhost/installation/base-info'
    sleep 2

    echo "==> Stopping after database creating"
    kill -SIGTERM ${pid} > /dev/null 2>&1 | true

    if [[ -n "${CLOUDRON_OIDC_ISSUER:-}" ]]; then
        echo "=> Disallow email registration"
        login_site_info=$(${DB_CMD_OPTIONS_SELECT} "SELECT content FROM site_info WHERE type='login' AND status=1" | \
            jq -c ".allow_email_registrations=false")
        ${DB_CMD_OPTIONS} "UPDATE site_info SET content='${login_site_info//\'/\'\'}' WHERE type='login' AND status=1"
    fi
fi

# https://github.com/apache/incubator-answer/blob/main/configs/config.yaml
echo "==> Update configuration"
cat /app/data/data/conf/config.yaml | \
    yq ".data.database.connection=\"${CLOUDRON_MYSQL_USERNAME}:${CLOUDRON_MYSQL_PASSWORD}@tcp(${CLOUDRON_MYSQL_HOST}:${CLOUDRON_MYSQL_PORT})/${CLOUDRON_MYSQL_DATABASE}\"" | \
    yq ".server.http.addr=\"0.0.0.0:8000\"" | \
    yq ".swaggerui.show=false" | \
    sponge /app/data/data/conf/config.yaml

general_site_info=$(${DB_CMD_OPTIONS_SELECT} "SELECT content FROM site_info WHERE type='general' AND status=1" | \
    jq ".site_url=\"${CLOUDRON_APP_ORIGIN}\"" | \
    jq -c ".check_update=false")
${DB_CMD_OPTIONS} "UPDATE site_info SET content='${general_site_info//\'/\'\'}' WHERE type='general' AND status=1"

if [[ -f /app/data/data/cache/cache.db ]]; then
    echo "==> Clear local cache"
    rm /app/data/data/cache/cache.db && touch /app/data/data/cache/cache.db
fi

echo "==> Upgrade Answer"
/app/code/answer upgrade

echo "==> Update SMTP settings"
smtp_settings=$(${DB_CMD_OPTIONS_SELECT} "SELECT value FROM config WHERE \`key\`='email.config'" | jq ".from_email=\"${CLOUDRON_MAIL_FROM}\"" | jq ".from_name=\"${CLOUDRON_MAIL_FROM_DISPLAY_NAME:-Answer}\"" | jq ".smtp_host=\"${CLOUDRON_MAIL_SMTP_SERVER}\"" | jq ".smtp_port=${CLOUDRON_MAIL_SMTPS_PORT}" | jq ".encryption=\"SSL\"" | jq ".smtp_username=\"${CLOUDRON_MAIL_SMTP_USERNAME}\"" | jq ".smtp_password=\"${CLOUDRON_MAIL_SMTP_PASSWORD}\"" | jq -c ".smtp_authentication=true")
${DB_CMD_OPTIONS} "UPDATE config SET value='${smtp_settings//\'/\'\'}' WHERE \`key\`='email.config'"

plugin_status=$(${DB_CMD_OPTIONS_SELECT} "SELECT value FROM config WHERE \`key\`='plugin.status'")

echo "==> Clear Redis cache"
redis-cli -h "${CLOUDRON_REDIS_HOST}" -p "${CLOUDRON_REDIS_PORT}" -a "${CLOUDRON_REDIS_PASSWORD}" --no-auth-warning flushall

echo "==> Update Redis settings"
plugin_status=$(echo ${plugin_status} | jq -c ".redis_cache=true")
${DB_CMD_OPTIONS} "UPDATE config SET value='${plugin_status//\'/\'\'}' WHERE \`key\`='plugin.status'"
${DB_CMD_OPTIONS} "REPLACE INTO plugin_config (plugin_slug_name, value) VALUES ('redis_cache', '{\"endpoint\":\"${CLOUDRON_REDIS_HOST}:${CLOUDRON_REDIS_PORT}\",\"password\":\"${CLOUDRON_REDIS_PASSWORD}\",\"username\":\"default\"}')"

if [[ -n "${CLOUDRON_OIDC_ISSUER:-}" ]]; then
    echo "==> Configuring OIDC auth"
    plugin_status=$(echo ${plugin_status} | jq -c ".basic_connector=true")

    provider_name="${CLOUDRON_OIDC_PROVIDER_NAME:-Cloudron}"

    ${DB_CMD_OPTIONS} "UPDATE config SET value='${plugin_status//\'/\'\'}' WHERE \`key\`='plugin.status'"
    ${DB_CMD_OPTIONS} "REPLACE INTO plugin_config (plugin_slug_name, value) VALUES ('basic_connector', '{\"authorize_url\":\"${CLOUDRON_OIDC_AUTH_ENDPOINT}\",\"check_email_verified\":true,\"client_id\":\"${CLOUDRON_OIDC_CLIENT_ID}\",\"client_secret\":\"${CLOUDRON_OIDC_CLIENT_SECRET}\",\"email_verified_json_path\":\"email_verified\",\"logo_svg\":\"\",\"name\":\"${provider_name//\'/\'\'}\",\"scope\":\"openid,profile,email\",\"token_url\":\"${CLOUDRON_OIDC_TOKEN_ENDPOINT}\",\"user_avatar_json_path\":\"\",\"user_display_name_json_path\":\"name\",\"user_email_json_path\":\"email\",\"user_id_json_path\":\"sub\",\"user_json_url\":\"${CLOUDRON_OIDC_PROFILE_ENDPOINT}\",\"user_username_json_path\":\"preferred_username\"}')"
fi

echo "==> Ensure permissions"
chown -R cloudron:cloudron /app/data

echo "==> Starting Answer"
exec gosu cloudron:cloudron /app/code/answer run -C /app/data/data
