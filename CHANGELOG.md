[0.1.0]
* Initial version for Apache Answer

[0.2.0]
* Update manifest

[0.3.0]
* Configure db on every start
* Fix memory limit

[0.4.0]
* Switch database to MySQL . **Please reinstall**
* OIDC support

[0.5.0]
* Update Apache Answer to 1.3.1
* [Full changelog](https://github.com/apache/incubator-answer/releases/tag/v1.3.1)

[1.0.0]
* Update Apache Answer to 1.3.5
* [Full changelog](https://github.com/apache/incubator-answer/releases/tag/v1.3.5)
* New: Add support for parametrizing the base path (@shuashuai #335)
* New: Add reactions to question & answer (@hgaol @shuashuai @LinkinStars #883)
* New: Embed plugin (@robinv8 @kumfo incubator-answer-plugins#84)
* Improve: Old questions may continue to be showed in the front (@sy-records #974)
* Improve: Admin able to update email of user (@sy-records #853)

[1.0.1]
* Update Apache Answer to 1.3.6
* [Full Changelog](https://github.com/apache/incubator-answer/releases/tag/v1.3.6)
* New: Support for pre-fill forms on the ask page ([@&#8203;hgaol](https://github.com/hgaol) [#&#8203;1009](https://github.com/apache/incubator-answer/issues/1009))
* New: Two CDN plugins to speed up your website ([@&#8203;kumfo](https://github.com/kumfo) [@&#8203;robinv8](https://github.com/robinv8) https://github.com/apache/incubator-answer-plugins/issues/128 https://github.com/apache/incubator-answer-plugins/issues/129)
* Improve: Issue with reading the Search engines meta verifications tags ([@&#8203;shuashuai](https://github.com/shuashuai) [@&#8203;LinkinStars](https://github.com/LinkinStars) [#&#8203;895](https://github.com/apache/incubator-answer/issues/895))
* Improve: After successfully installing Answer, enter the 50x page ([@&#8203;LinkinStars](https://github.com/LinkinStars) [#&#8203;997](https://github.com/apache/incubator-answer/issues/997))
* Improve: Improve "answer your own question" checkbox ([@&#8203;shuashuai](https://github.com/shuashuai) [#&#8203;1032](https://github.com/apache/incubator-answer/issues/1032))
* Improve: Improve the algorithm for popular questions ([@&#8203;kumfo](https://github.com/kumfo) [#&#8203;1033](https://github.com/apache/incubator-answer/issues/1033))
* Improve: Show all recommend tags in ask page ([@&#8203;shuashuai](https://github.com/shuashuai) [#&#8203;1035](https://github.com/apache/incubator-answer/issues/1035))
* Improve: Improve diff style ([@&#8203;shuashuai](https://github.com/shuashuai) [#&#8203;1011](https://github.com/apache/incubator-answer/issues/1011))
* Improve: Getting Error, TAG not found. When site settings don't require tags for question creation ([@&#8203;shuashuai](https://github.com/shuashuai) [#&#8203;815](https://github.com/apache/incubator-answer/issues/815))
* Improve: Add search icon in search bar ([@&#8203;shuashuai](https://github.com/shuashuai) [#&#8203;1010](https://github.com/apache/incubator-answer/issues/1010))
* Improve: Admin able to update display name of user ([@&#8203;sy-records](https://github.com/sy-records) [#&#8203;1024](https://github.com/apache/incubator-answer/issues/1024))
* Fixed: When `@` someone, markdown render error ([@&#8203;sy-records](https://github.com/sy-records) [#&#8203;978](https://github.com/apache/incubator-answer/issues/978))
* Fixed: The website information in the template file should support multiple languages ([@&#8203;kevingil](https://github.com/kevingil) [#&#8203;998](https://github.com/apache/incubator-answer/issues/998))
* Fixed: The debug plugin i18n cannot be loaded normally ([@&#8203;LinkinStars](https://github.com/LinkinStars) [#&#8203;1000](https://github.com/apache/incubator-answer/issues/1000))
* Fixed: Search result tab order ([@&#8203;shuashuai](https://github.com/shuashuai) [#&#8203;1031](https://github.com/apache/incubator-answer/issues/1031))
* Fixed: Enabling the "required tag" without setting it will result in inability to ask questions ([@&#8203;LinkinStars](https://github.com/LinkinStars) [#&#8203;1034](https://github.com/apache/incubator-answer/issues/1034))
* Fixed: The number of answers is not updated, after someone's answer has been approved ([@&#8203;LinkinStars](https://github.com/LinkinStars) [#&#8203;1022](https://github.com/apache/incubator-answer/issues/1022))
* Fixed: The images in the image upload component are forced to enlarge ([@&#8203;shuashuai](https://github.com/shuashuai) [#&#8203;1039](https://github.com/apache/incubator-answer/issues/1039))
* Fixed: The color of the hyperlinks filled in the answers is difficult to distinguish from the background color ([@&#8203;shuashuai](https://github.com/shuashuai) [#&#8203;1012](https://github.com/apache/incubator-answer/issues/1012))

[1.1.0]
* Update Apache Answer to 1.4.0
* [Full Changelog](https://github.com/apache/incubator-answer/releases/tag/v1.4.0)
* New: Badges, weve gamified the Q\&A platform with badges. You can also write more rules to award the badges ([@&#8203;kumfo](https://github.com/kumfo) [@&#8203;LinkinStars](https://github.com/LinkinStars) [@&#8203;shuashuai](https://github.com/shuashuai) [@&#8203;robinv8](https://github.com/robinv8) [#&#8203;1036](https://github.com/apache/incubator-answer/issues/1036))
* New: Define Apache Answer search provider to add to the browser ([@&#8203;sy-records](https://github.com/sy-records) [#&#8203;1058](https://github.com/apache/incubator-answer/issues/1058))
* New: Question list for personalized recommendations ([@&#8203;ferrischi201](https://github.com/ferrischi201) [#&#8203;1003](https://github.com/apache/incubator-answer/issues/1003))
* New: Support Render type plugins([@&#8203;robinv8](https://github.com/robinv8) [#&#8203;1086](https://github.com/apache/incubator-answer/issues/1086))
* New: Code highlighting plugin([@&#8203;IamMelody233](https://github.com/IamMelody233) [@&#8203;CHENJUaaa](https://github.com/CHENJUaaa) https://github.com/apache/incubator-answer-plugins/pull/213)
* Fixed: The reaction in mobile phone is displayed incorrectly ([@&#8203;SantiagoLiendro](https://github.com/SantiagoLiendro) [#&#8203;1064](https://github.com/apache/incubator-answer/issues/1064))
* Fixed: There are extra characters in the html source code ([@&#8203;sosyz](https://github.com/sosyz) [#&#8203;1066](https://github.com/apache/incubator-answer/issues/1066))
* Fixed: When a question has only one answer and this answer is deleted, the question will not display in the unanswered list ([@&#8203;sosyz](https://github.com/sosyz) [#&#8203;1062](https://github.com/apache/incubator-answer/issues/1062))
* Fixed: Parameter error in VerifyCode Test Case  ([@&#8203;sosyz](https://github.com/sosyz) [#&#8203;1059](https://github.com/apache/incubator-answer/issues/1059))
* Fixed: `Embed` plugin type conflict ([@&#8203;kumfo](https://github.com/kumfo) https://github.com/apache/incubator-answer-plugins/issues/189 )
* Fixed: When deploying using subdirectories, some links are missing the base_url prefix [@&#8203;shuashuai](https://github.com/shuashuai)

[1.1.1]
* Update incubator-answer to 1.4.1
* [Full Changelog](https://github.com/apache/incubator-answer/releases/tag/v1.4.1)
* New: Linking question ([@&#8203;sosyz](https://github.com/sosyz) [@&#8203;LinkinStars](https://github.com/LinkinStars) [@&#8203;robinv8](https://github.com/robinv8)  [#&#8203;817](https://github.com/apache/incubator-answer/issues/817))
* Improve: Remove the minimum username length restriction ([@&#8203;zahash](https://github.com/zahash) [#&#8203;1124](https://github.com/apache/incubator-answer/issues/1124))
* Improve: Improve the display of personal page when there is no information ([@&#8203;sy-records](https://github.com/sy-records) [#&#8203;1104](https://github.com/apache/incubator-answer/issues/1104))
* Improve: Installation page supports i18n ([@&#8203;sy-records](https://github.com/sy-records) [#&#8203;1077](https://github.com/apache/incubator-answer/issues/1077))

[1.1.2]
* checklist added to CloudronManifest
* COUDRON_OIDC_PROVIDER_NAME implemented

[1.1.3]
* Update incubator-answer to 1.4.2
* [Full Changelog](https://github.com/apache/incubator-answer/releases/tag/v1.4.2)
* New: Need attachment-upload feature ([@&#8203;LinkinStars](https://github.com/LinkinStars) [@&#8203;shuashuai](https://github.com/shuashuai) [#&#8203;211](https://github.com/apache/incubator-answer/issues/211))
* New: Add key metrics to the dashboard ([@&#8203;sy-records](https://github.com/sy-records) [#&#8203;1173](https://github.com/apache/incubator-answer/issues/1173))
* New: Add "Frequent" tab to filter most linked question ([@&#8203;LinkinStars](https://github.com/LinkinStars) [@&#8203;shuashuai](https://github.com/shuashuai) [#&#8203;1193](https://github.com/apache/incubator-answer/issues/1193))
* Improve: When the code snippet is very long, the page is not easy to read ([@&#8203;shuashuai](https://github.com/shuashuai) [#&#8203;1168](https://github.com/apache/incubator-answer/issues/1168))
* Improve: Add a "Do not reply directly" message to the reminder email ([@&#8203;sosyz](https://github.com/sosyz) [#&#8203;1170](https://github.com/apache/incubator-answer/issues/1170))
* Improve: Enhance user homepage SEO content ([@&#8203;LinkinStars](https://github.com/LinkinStars) [#&#8203;1182](https://github.com/apache/incubator-answer/issues/1182))
* Improve: Remove "Heading 1" in markdown editor ([@&#8203;robinv8](https://github.com/robinv8) [#&#8203;1194](https://github.com/apache/incubator-answer/issues/1194))
* Improve: Tag summary display is not as expected on tag detail page ([@&#8203;sy-records](https://github.com/sy-records) [#&#8203;1169](https://github.com/apache/incubator-answer/issues/1169))
* Fixed: Markdown parsing without incrementing ordered list numbers ([@&#8203;sy-records](https://github.com/sy-records) [#&#8203;1174](https://github.com/apache/incubator-answer/issues/1174))
* Fixed: Change list/unlist causes tag display exception ([@&#8203;sosyz](https://github.com/sosyz) [#&#8203;1144](https://github.com/apache/incubator-answer/issues/1144))
* Fixed: Tags static page for SEO ([@&#8203;sy-records](https://github.com/sy-records) [#&#8203;1158](https://github.com/apache/incubator-answer/issues/1158))
* Fixed: The timezone display on the Dashboard page is empty when the timezone is set to UTC ([@&#8203;sy-records](https://github.com/sy-records) [#&#8203;1159](https://github.com/apache/incubator-answer/issues/1159))
* Fixed: Duplication of content on the results page ([@&#8203;LinkinStars](https://github.com/LinkinStars) [#&#8203;1166](https://github.com/apache/incubator-answer/issues/1166))
* Fixed: User Addition Fails Due to Unexpected Username Length Restriction in Admin Panel ([@&#8203;sy-records](https://github.com/sy-records) [#&#8203;1189](https://github.com/apache/incubator-answer/issues/1189))

[1.2.0]
* Update to base image 5.0.0

[1.3.0]
* add google connector

