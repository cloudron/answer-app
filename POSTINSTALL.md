This app is pre-setup with an admin account. The initial credentials are:

**Email**: admin@cloudron.local<br/>
**Password**: changeme123<br/>

<sso>
By default, Cloudron users have `User` role. The user role can be changed on the users management page.
</sso>
