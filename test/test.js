#!/usr/bin/env node

/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.EMAIL || !process.env.PASSWORD) {
    console.log('EMAIL, PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 50000;

    let browser, app, cloudronName;

    const EMAIL = process.env.EMAIL;
    const PASSWORD = process.env.PASSWORD;

    const ADMIN_EMAIL = 'admin@cloudron.local';
    const ADMIN_PASSWORD = 'changeme123';

    const QUESTION_TAG = 'DarkAngelColt';

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TIMEOUT);
    }

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION || a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');

        const tmp = execSync(`cloudron exec --app ${app.id} env`).toString().split('\n').find((l) => l.indexOf('CLOUDRON_OIDC_PROVIDER_NAME=') === 0);
        if (tmp) cloudronName = tmp.slice('CLOUDRON_OIDC_PROVIDER_NAME='.length);
    }

    async function login(email, password) {
        await browser.manage().deleteAllCookies();
        await browser.sleep(2000);

        await browser.get(`https://${app.fqdn}/users/login`);

        await waitForElement(By.id('email'));
        await browser.findElement(By.id('email')).sendKeys(email);
        await browser.findElement(By.id('pass')).sendKeys(password);
        await browser.findElement(By.xpath('//button[text()="Log in"]')).click();
        await waitForElement(By.xpath('//span[text()="Questions"]'));
    }

    async function loginOIDC(username, password, alreadyAuthenticated) {
        browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}/users/login`);
        await browser.sleep(2000);

        await browser.wait(until.elementLocated(By.xpath(`//a[contains(., "Connect with ${cloudronName}")]`)), TIMEOUT);
        await browser.findElement(By.xpath(`//a[contains(., "Connect with ${cloudronName}")]`)).click();
        await browser.sleep(2000);

        if (!alreadyAuthenticated) {
            await waitForElement(By.xpath('//input[@name="username"]'));
            await browser.findElement(By.xpath('//input[@name="username"]')).sendKeys(username);
            await browser.findElement(By.xpath('//input[@name="password"]')).sendKeys(password);
            await browser.sleep(2000);
            await browser.findElement(By.xpath('//button[@type="submit" and contains(text(), "Sign in")]')).click();
            await browser.sleep(2000);
        }

        await waitForElement(By.xpath('//span[text()="Questions"]'));
    }

    async function logout() {
        await browser.get(`https://${app.fqdn}/users/logout`);
        await waitForElement(By.id('email'));
    }

    async function getMainPage() {
        await browser.get('https://' + app.fqdn);
        await browser.sleep(3000);
        await waitForElement(By.xpath('//span[text()="Questions"]'));
    }

    async function addTag() {
        await browser.get(`https://${app.fqdn}/tags/create`);
        await waitForElement(By.id('display_name'));
        await browser.findElement(By.id('display_name')).sendKeys(QUESTION_TAG);
        await browser.findElement(By.id('slug_name')).sendKeys('some-slug');
        await browser.findElement(By.xpath('//button[contains(@title, "Strong")]')).click(); // add description
        await browser.findElement(By.xpath('//button[text()="Post new tag"]')).click();
        await browser.sleep(2000);
        await waitForElement(By.xpath('//a[@href="/tags/some-slug"]'));
    }

    async function checkTag() {
        await browser.get(`https://${app.fqdn}/tags`);
        await waitForElement(By.xpath('//a[@href="/tags/some-slug"]'));
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });

    it('install app (no sso)', function () { execSync(`cloudron install --no-sso --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can admin login', login.bind(null, ADMIN_EMAIL, ADMIN_PASSWORD));
    it('can get the main page', getMainPage);

    it('can add tag', addTag);
    it('check tag', checkTag);

    it('uninstall app (no sso)', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    it('install app (sso)', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can admin login', login.bind(null, ADMIN_EMAIL, ADMIN_PASSWORD));
    it('can get the main page', getMainPage);

    it('can add tag', addTag);
    it('check tag', checkTag);
    it('can admin logout', logout);

    it('can login', loginOIDC.bind(null, EMAIL, PASSWORD, false));
    it('can get the main page', getMainPage);

    it('check tag', checkTag);
    it('can logout', logout);

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS); });
    it('can admin login', login.bind(null, ADMIN_EMAIL, ADMIN_PASSWORD));
    it('check tag', checkTag);
    it('can logout', logout);

    it('can login', loginOIDC.bind(null, EMAIL, PASSWORD, true));
    it('check tag', checkTag);
    it('can logout', logout);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', async function () {
        await browser.get('about:blank');
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);
    it('can admin login', login.bind(null, ADMIN_EMAIL, ADMIN_PASSWORD));
    it('check tag', checkTag);
    it('can logout', logout);

    it('can login', loginOIDC.bind(null, EMAIL, PASSWORD, true));
    it('check tag', checkTag);
    it('can logout', logout);

    it('move to different location', async function () {
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
        await browser.sleep(5000);
    });
    it('can get app information', getAppInfo);
    it('can admin login', login.bind(null, ADMIN_EMAIL, ADMIN_PASSWORD));
    it('check tag', checkTag);
    it('can logout', logout);

    it('can login', loginOIDC.bind(null, EMAIL, PASSWORD, true));
    it('check tag', checkTag);
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('can install app for update', async function () {
        execSync(`cloudron install --appstore-id org.apache.answer.cloudronapp --location ${LOCATION}`, EXEC_ARGS);
    });
    it('can get app information', getAppInfo);

    it('can admin login', login.bind(null, ADMIN_EMAIL, ADMIN_PASSWORD));
    it('can add tag', addTag);
    it('check tag', checkTag);

    it('can update', function () { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can admin login', login.bind(null, ADMIN_EMAIL, ADMIN_PASSWORD));
    it('check tag', checkTag);
    it('can logout', logout);

    it('can login', loginOIDC.bind(null, EMAIL, PASSWORD, true));
    it('check tag', checkTag);
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});

